module Parser where

import Parsing
import Actions
import World

{- The structure of these functions and their usage of the Parsing.hs file was adapted from the lecture notes on parsing found here: 
https://studres.cs.st-andrews.ac.uk/CS2006/Lectures/Haskell/W2-10-ParsingExample.pdf
-}


-- | 'action' Breaks down a command and tries to parse as a movement command, an instruction with an object, a terminal command, or io command
action :: Parser Actions
action = do a <- goCommand
            return a
           ||| do obj <- objCommand
                  return obj
                 ||| do c <- command
                        return c
                      ||| do io <- iocommand
                             return io
                           ||| failure 


-- | 'objCommand' attempts to parse an action which takes an object as a paremeter
objCommand :: Parser Actions
objCommand = do c <- pactions
                do _ <- space
                   obj <- object
                   -- Need to convert as we can't initialise an Action type without an argument
                   return ( convertToType c obj )


-- | 'object' parses a simple object used as an argument for any command 
object :: Parser Object
object = do c <- pobjects
            return c


-- | 'goCommand' attempts to parse a movement action with go + a direction
goCommand :: Parser Actions
goCommand = do _ <- pgo
               do _ <- space
                  d <- Parser.direction
                  return (Go d)
               ||| failure 

-- | evaluates a save and load command
iocommand :: Parser Actions
iocommand = do io <- pio
               do _ <- space
                  str <- ident
                  return (convertToIOCommand io str)
                ||| failure


-- | 'convertToType' converts a given string representing a command and an object argument to an action type
convertToType :: String -> Object -> Actions
convertToType "get" obj = Get obj
convertToType "pour" obj = Pour obj
convertToType "drop" obj = Drop obj
convertToType "examine" obj = Examine obj
convertToType "open" obj = Open obj
convertToType "drink" obj = Drink obj
convertToType "close" obj = Close obj
convertToType _ _ = UnknownAction


convertToIOCommand :: String -> String -> Actions
convertToIOCommand "save" str = Save str
convertToIOCommand "load" str = Load str
convertToIOCommand _ _        = UnknownAction



-- | 'command' attempts to parse the possible commands (Quit, Inventory)
command :: Parser Actions
command = do c <- pcommands
             return c


-- | 'direction' attempts to parse a direction
direction :: Parser Direction
direction = do dir <- pdirections
               return dir


-- | 'eval' initiates the parsing and takes the Actions type that is returned
eval :: String -> Actions
eval xs = fst (head'(parse action xs))

-- | 'head'' safe head which can only be used for the output from parsing
head' :: [(Actions, String)] -> (Actions, String)
head' [] = (UnknownAction, "")
head' (x:xs) = x





{- Test functions 
evalCommand :: String -> [(Actions, String)]
evalCommand xs = (parse command xs)


evalGo :: String -> [(Actions, String)]
evalGo xs = (parse goCommand xs)


evalObj :: String -> [(Actions, String)]
evalObj xs = (parse objCommand xs)

evalIO :: String -> [(Actions, String)]
evalIO xs = (parse iocommand xs)  -}

