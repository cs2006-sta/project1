{-
Functional parsing library from chapter 8 of Programming in Haskell,
Graham Hutton, Cambridge University Press, 2007.

Minor changes by Edwin Brady
-}

module Parsing where

import Data.Char
import Control.Monad
import Control.Applicative hiding (many)
import Actions
import World

infixr 5 |||

{-
The monad of parsers
--------------------
-}

newtype Parser a              =  P (String -> [(a,String)])

instance Functor Parser where
   fmap f p = do p' <- p
                 return (f p')

instance Applicative Parser where
   pure = return
   f <*> a = do f' <- f
                a' <- a
                return (f' a')

instance Monad Parser where
   return v                   =  P (\inp -> [(v,inp)])
   p >>= f                    =  P (\inp -> case parse p inp of
                                               []        -> []
                                               [(v,out)] -> parse (f v) out)

instance Alternative Parser where
   empty = mzero
   p <|> q = p ||| q

instance MonadPlus Parser where
   mzero                      =  P (\inp -> [])
   p `mplus` q                =  P (\inp -> case parse p inp of
                                               []        -> parse q inp
                                               [(v,out)] -> [(v,out)])

{-
Basic parsers
-------------
-}

failure                       :: Parser a
failure                       =  mzero

item                          :: Parser Char
item                          =  P (\inp -> case inp of
                                               []     -> []
                                               (x:xs) -> [(x,xs)])

parse                         :: Parser a -> String -> [(a,String)]
parse (P p) inp               =  p inp

{-
Choice
------
-}

(|||)                         :: Parser a -> Parser a -> Parser a
p ||| q                       =  p `mplus` q

{-
Derived primitives
------------------
-}

sat                           :: (Char -> Bool) -> Parser Char
sat p                         =  do x <- item
                                    if p x then return x else failure

digit                         :: Parser Char
digit                         =  sat isDigit

lower                         :: Parser Char
lower                         =  sat isLower

upper                         :: Parser Char
upper                         =  sat isUpper

letter                        :: Parser Char
letter                        =  sat isAlpha

alphanum                      :: Parser Char
alphanum                      =  sat isAlphaNum

char                          :: Char -> Parser Char
char x                        =  sat (== x)

string                        :: String -> Parser String
string []                     =  return []
string (x:xs)                 =  do char x
                                    string xs
                                    return (x:xs)

many                          :: Parser a -> Parser [a]
many p                        =  many1 p ||| return []

many1                         :: Parser a -> Parser [a]
many1 p                       =  do v  <- p
                                    vs <- many p
                                    return (v:vs)

ident                         :: Parser String
ident                         =  do x  <- lower
                                    xs <- many alphanum
                                    return (x:xs)

nat                           :: Parser Int
nat                           =  do xs <- many1 digit
                                    return (read xs)

int                           :: Parser Int
int                           =  do char '-'
                                    n <- nat
                                    return (-n)
                                  ||| nat

-- New parser functions for validating possible commands

-- | Attempts to parse a 'quit' or 'inv' command
pcommands                       :: Parser Actions
pcommands                       = do str <- ident 
                                     let command = valcom ( str )
                                     if (command) == UnknownAction then failure else return command


-- | Attempts to parse a 'Direction'
pdirections                     :: Parser Direction
pdirections                     = do str <- ident
                                     if (valdir str) == UnknownDirection then failure else return (valdir str)


-- | Attempts to parse a game 'Object'. This version attempts to parse 'coffee' before trying 'coffee pot'
pobjects'                        :: Parser Object
pobjects'                        = do str <- ident
                                      if (valobj str) == UnknownObject then
                                        do s <- space
                                           nextStr <- ident
                                           if valobj (str ++ " " ++ nextStr) == UnknownObject then failure else return (valobj (str ++ " " ++ nextStr))
                                         ||| failure
                                      else return (valobj str)

-- | Attempts to parse a game 'Object'. This version will attempts to parse 'coffee pot' before 'coffee'
pobjects                       :: Parser Object
pobjects                       = do str <- ident
                                    do s <- space
                                       nextStr <- ident
                                       if valobj ( str ++ " " ++ nextStr ) == UnknownObject then failure else return (valobj( str ++ " " ++ nextStr ))
                                     ||| if valobj ( str ) == UnknownObject  then failure else return (valobj str )  
                                   ||| failure

-- | Attempts to parse a particular 'Actions' e.g. drink, pour etc. 
pactions                        :: Parser String
pactions                        = do str <- ident
                                     if (valact str) == "" then failure else return str


-- | Does the same as 'pactions' but only looks for 'go'
pgo                             :: Parser String
pgo                             = do str <- ident
                                     if str /= "go" then failure else return str

-- | Validates to see if the command is 'save' or 'load'
pio                             :: Parser String
pio                              = do str <- ident
                                      if (valio str) == "" then failure else return str




space                         :: Parser ()
space                         =  do many (sat isSpace)
                                    return ()





{-
Ignoring spacing
----------------
-}

token                         :: Parser a -> Parser a
token p                       =  do space
                                    v <- p
                                    space
                                    return v

identifier                    :: Parser String
identifier                    =  token ident

natural                       :: Parser Int
natural                       =  token nat

integer                       :: Parser Int
integer                       =  token int

symbol                        :: String -> Parser String
symbol xs                     =  token (string xs)

-- Combination rules

-- | Just echoes back the string if it matches a 'Command'
valcom :: String -> Actions
valcom "inv"      =  Inventory
valcom "inventory" = Inventory
valcom "help"     = Help
valcom "quit"     =  Quit
valcom "exit"     = Quit
valcom _          = UnknownAction

-- | Pattern matches on the save and load strings
valio :: String -> String
valio "save" = "save"
valio "load" = "load"
valio _ = ""


-- | Just echoes back the string if it matches an action
valact :: String -> String
valact "get"     =  "get"
valact "drop"    =  "drop"
valact "pour"    =  "pour"
valact "examine" =  "examine"
valact "drink"   =  "drink"
valact "open"    =  "open"
valact "close"   = "close"
valact _         = ""

-- | Returns a 'Direction' if it can match a valid direction
valdir :: String -> Direction
valdir "north" = North
valdir "south" = South
valdir "east" = East
valdir "west" = West
valdir "outside" = Out
valdir "out"  = Out
valdir "in"    = In
valdir "inside"  = In
valdir _      = UnknownDirection

-- | Returns an 'Object' if it can match a valid object
valobj :: String -> Object
valobj "silver key" = Key
valobj "door key" = Key
valobj "key"      = Key
valobj "mug" = Mug
valobj "empty mug" = Mug
valobj "coffee mug" = CoffeeMug
valobj "full mug"   = CoffeeMug
valobj "full cup"   = CoffeeMug
valobj "coffee cup" = CoffeeMug
valobj "coffee pot" = CoffeePot
valobj "door"        = Door
valobj "coffee"      = CoffeeMug
valobj "pot"        = CoffeePot
valobj "house key"  = Key
valobj "tooth brush" = ToothBrush
valobj "toothbrush" = ToothBrush
valobj "brush" = ToothBrush
valobj _            = UnknownObject