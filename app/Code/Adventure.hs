module Main where

import World
import Actions
import Parser
import IOActions

import Control.Monad.Trans.Class
import System.IO
import System.Console.Haskeline

-- | Simple messages
winmessage, seperator, lostmessage :: String
winmessage = "Congratulations, you have made it outside and locked the door!\n"
lostmessage = "Game over! You got locked out of your house!"
seperator = "\n"

-- | Pattern matches IO actions
-- returns a boolean which determines whether a new game has been started
-- if one has, then it must terminate when that game finishes otherwise it will go back to the original state before the save or load
-- In the event a load file cannot be found it will start a new game with the current state as if the user hasn't done anything
ioactions :: Actions -> GameData -> IO Bool
ioactions (Save str) state = do save str state
                                _ <- repl initState
                                return True
ioactions (Load str) state = do newstate <- load str state
                                _ <- repl newstate
                                return True
ioactions (UnknownAction) _ = do return False
ioactions _ _ = do return False


{- Given a game state, and user input (as a list of words) return a 
   new game state and a message for the user. -}


-- Process now will only be given valid input, the parser disallows any invalid data so with any input, we should get the correct
-- response from pattern matching on actions
process :: GameData -> Actions -> (GameData, String)
process state cmd = actions cmd state



-- | 'repl' is the game loop which processes various different game states
repl :: GameData -> IO GameData
repl state | finished state = return state
repl state = do print state
                putStr "What now? "
                hFlush stdout
                cmd <- getLine
                let com = eval cmd
                newgame <- ioactions com state
                if (newgame) then return state
                             else do
                                    --Call eval to start the parser in Parser.hs
                                    let (state', msg') = process state (com)
                                    putStrLn seperator
                                    putStrLn msg'
                                    if (won state') then do putStrLn winmessage
                                                            return state'
                                    else if (lost state') then do putStrLn lostmessage
                                                                  return state'
                                                          else repl state'

-- | Re 'main'
main' :: IO ()
main' = do _ <- repl initState
           return ()





{-
The basic structure of the Hasleine implemetation was adapted from here:
https://hackage.haskell.org/package/haskeline-0.8.1.0/docs/System-Console-Haskeline.html
-}

-- | the main game loop using Haskeline
repl' :: GameData -> IO()
repl' state = runInputT defaultSettings (loop state "")
              where
                 -- Haskeline runs in InputT IO so any IO functions need to be lifted
              loop :: GameData -> String -> InputT IO()
              loop state msg | finished state = do outputStrLn $ msg
                                                   return ()
              loop state msg = do outputStrLn $ msg
                                  lift(print state)
                                  minput <- getInputLine "> "
                                  -- Maybe String for input
                                  case minput of
                                     -- Just re print the state
                                       Nothing -> lift (print state)
                                       Just input -> do let com = eval input
                                                      -- the boolean determines if a new game has been started so we end this loop if one has
                                                        bool <- lift (ioactions' com state)
                                                        if (bool) then do return ()
                                                                  else do
                                                                     -- Continue with game ( get new state and msg )
                                                                        let (state', msg) = process state (com)
                                                                        outputStrLn $ seperator
                                                                        if (won state') then do outputStrLn $ winmessage
                                                                                                return ()
                                                                                       else if (lost state') then do outputStrLn lostmessage
                                                                                                                     return ()
                                                                                                            else loop state' msg


-- | IO action pattern matcher for the Haskeline solution
ioactions' :: Actions -> GameData -> IO Bool
ioactions' (Save str) state = do save str state
                                 _ <- repl' initState
                                 -- new game has started with new state ( game saved )
                                 return (True)
ioactions' (Load str) state = do newstate <- load str state
                                 _ <- repl' newstate
                                 -- new game has started with a loaded state so return true
                                 return (True)

-- Return true for these two cases
ioactions' (UnknownAction) _ = do return False
ioactions' _ _ = do return False

-- | Initiates the game loop
main :: IO()
main = do
          _ <- repl' initState
          return ()