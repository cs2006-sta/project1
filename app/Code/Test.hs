{-# LANGUAGE TemplateHaskell #-}
module Main where

import Test.QuickCheck
import Test.QuickCheck.All
import Parsing
import Actions
import World
import Parser

return []
main :: IO Bool
main = $quickCheckAll

instance Arbitrary GameData where 
    arbitrary = elements[initState]


instance Arbitrary Room where 
    arbitrary = elements [bedroom, kitchen, hall, street, livingroom, toilet]

instance Arbitrary Object where
    arbitrary = elements [Mug, CoffeeMug, CoffeePot, Door, Key, ToothBrush]

instance Arbitrary Direction where  
    arbitrary = elements [ North, South, East, West, Out, In]

prop_addInventory gameData object = 
    length (inventory (addInv gameData object)) >= length (inventory gameData)

prop_removeInventory gameData object = 
    length (inventory (removeInv gameData object)) <= length (inventory gameData)


{- Given GameData and a direction, tests to see if the exits that the room has work and that you cannot access the directions where 
    there are no exits in that room -}
prop_navigate room direction = expected room direction == result room direction where 
    expected :: Room -> Direction -> Bool
    expected room direction = containsDirection direction (exits room) where
        containsDirection :: World.Direction -> [Exit] -> Bool
        containsDirection direction roomExits = or ( map (\roomExit -> direction == ( type_dir (roomExit) ) ) roomExits)

    result :: Room -> Direction -> Bool
    result gameData direction = case (move direction room) of Nothing -> False
                                                              _ -> True
    
