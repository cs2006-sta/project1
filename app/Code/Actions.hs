module Actions where

import World
import Data.List (find)

{- | Message returned to the user if a command doesn't make sense -}
errormessage :: String
errormessage = "I don't understand - Use 'help' for a guide on how to play"



-- | 'actions' pattern matches the possible parsed actions and runs the according function
actions :: Actions -> GameData -> (GameData, String)
actions (Go dir) state = go dir state
actions (Get obj) state = get obj state
actions (Drop obj) state = put obj state
actions (Pour obj) state = pour obj state
actions (Examine obj) state = examine obj state
actions (Drink obj) state = drink obj state
actions (Open obj) state = open obj state
actions (Quit) state = quit state
actions (Inventory) state = inv state
actions (Close obj) state = close obj state
actions (Help) state = help state
actions (UnknownAction) state = (state, errormessage)
actions _ state = (state, errormessage)


-- | Actions to allow interaction with the game world
data Actions = Go Direction | Get Object | Drop Object | Pour Object | Examine Object | Drink Object | Open Object | Close Object | Quit | Inventory | Help | Save String | Load String | UnknownAction
   deriving (Eq, Show)


-- | Given a direction and a room to move from, return the room id that direction, if it exists
move :: Direction -> Room -> Maybe String
move dir rm = case find (\e -> exit_dir e == show dir) (exits rm) of
               Just e -> Just (room e)
               Nothing -> Nothing

-- | Return True if the object appears in the room
objectHere :: Object -> Room -> Bool
objectHere o rm = o `elem` objects rm

-- | Given an object id and a room description, return a new room description without that object
removeObject :: Object -> Room -> Room
removeObject o rm = rm {objects = filter (/=o) (objects rm)}

-- | Given an object and a room description, return a new room description with that object added
addObject :: Object -> Room -> Room
addObject o rm = rm {objects = objects rm ++ [o]}
 
-- | Given a game state and a room id, replace the old room information with new data. If the room id does not already exist, add it.
updateRoom :: GameData -> String -> Room -> GameData
updateRoom gd rmid rmdata = gd {world = filter ((/=rmid).fst) (world gd) ++ [(rmid, rmdata)]}

-- | Given a game state and an object id, find the object in the current room and add it to the player's inventory
addInv :: GameData -> Object -> GameData
addInv gd obj = gd {inventory = inventory gd ++ [obj]} 

{-- | Given a game state and an object id, remove the object from the
   inventory. Hint: use filter to check if something should still be in
   the inventory. -}
removeInv :: GameData -> Object -> GameData
removeInv gd obj = gd {inventory = filter (/=obj) (inventory gd)}

{-  | Does the inventory in the game state contain the given object? -}
carrying :: GameData -> Object -> Bool
carrying gd obj = obj `elem` inventory gd

-- | Checks if the current game state of the user's game matches that with what determines a win
won :: GameData -> Bool
won gd = location_id gd == "street" && carrying gd Key && door_closed gd

-- | similiar to 'won' but for a lost state
lost :: GameData -> Bool
lost gd = location_id gd == "street" && not(carrying gd Key) && door_closed gd

{-  | Define the "go" action. Given a direction and a game state, update the game
   state with the new location. If there is no exit that way, report an error.
   Remember Actions return a 2-tuple of GameData and String. The String is
   a message reported to the player. -}
go :: Move
go dir state = let newRm = move dir (getRoomData state)
               in case newRm of 
                 Just rm -> (state {location_id = rm}, "Moved into the " ++ rm ++ "")
                 Nothing -> (state, "There is no exit in that direction")

{- | Remove an item from the current room, and put it in the player's inventory.
   This should only work if the object is in the current room. Use 'objectHere'
   and 'removeObject' to remove the object, and 'updateRoom' to replace the
   room in the game state with the new room which doesn't contain the object. -}
get :: Action
get obj state | getRoomData state == hall && obj == Door = (state, "You cannot pick up the front door")
              | not (objectHere obj (getRoomData state)) = (state, "That object is not in the current room")
              | otherwise = (updateRoom (addInv state obj) (location_id state) (removeObject obj (getRoomData state)), show obj ++ " added to inventory")

{- | Remove an item from the player's inventory, and put it in the current room.
   Similar to 'get' but in reverse - find the object in the inventory, create
   a new room with the object in, update the game world with the new room. -}
put :: Action
put obj state | not (carrying state obj) = (state, "That object is not in your inventory")
              | otherwise = (updateRoom (removeInv state obj) (location_id state) (addObject obj (getRoomData state)), "Put " ++ show obj ++ " in the " ++ location_id state ++ "")

{- | Don't update the state, just return a message giving the full description
   of the object. As long as it's either in the room or the player's 
   inventory! -}
examine :: Action
examine obj state | getRoomData state == hall && obj == Door = (state, objectDescription obj)
                  | not (objectHere obj (getRoomData state)) && not (carrying state obj) = (state, "That object is not in your inventory or in the current room")
                  | otherwise = (state, objectDescription obj)

{- | Pour the coffee. Obviously, this should only work if the player is carrying
   both the pot and the mug. This should update the status of the "mug"
   object in the player's inventory to be a new object, a "full mug". -}
pour :: Action
pour obj state | obj /= CoffeePot = (state, "You can only pour the coffee pot")
               | not (carrying state obj) = (state, "You do not have a coffee pot")
               | not (carrying state Mug) = (state, "You need an empty mug to pour the coffee into")
               | otherwise = ((addInv (removeInv state Mug) CoffeeMug){poured = True}, "You pour the coffee")                

{- | Drink the coffee. This should only work if the player has a full coffee 
   mug! Doing this is required to be allowed to open the door. Once it is
   done, also update the 'caffeinated' flag in the game state.

   Also, put the empty coffee mug back in the inventory! -}
drink :: Action
drink obj state | obj /= CoffeeMug = (state, "You can only drink a full mug of coffee")
                | not (carrying state obj) = (state, "You need to pour a mug of coffee first")
                | otherwise = ((addInv (removeInv state CoffeeMug) Mug){caffeinated = True}, "You drink the coffee")

{- | Open the door. Only allowed if the player has had coffee! 
   This should change the description of the hall to say that the door is open,
   and add an exit out to the street. -}
open :: Action
open obj state | obj /= Door = (state, "You can only open a door")
               | getRoomData state /= hall = (state, "You need to be in the hallway to open the front door")
               | not (door_closed state) = (state, "The door is already open")
               | not (carrying state Key) = (state, "You need the front door key to open the front door")
               | not (caffeinated state) = (state, "You need to drink some coffee before leaving")
               | otherwise = (exitstate state, "You open the door")

{- | Closes the door. Permitted if the user is outside -}
close :: Action
close obj state | obj /= Door = (state, "You can only close a door")
                | getRoomData state /= street = (state, "You need to be outside to close the front door")
                | otherwise = (winstate state, "You close the door")




{- initial state of the game at the beginning -}
initState :: GameData
initState = GameData "bedroom" gameworld [] False False False True

{- | The state we change to when the user opens the door ( the door is now not closed ) -}
exitstate :: GameData -> GameData
exitstate state = (fst(put Key (updateRoom state "hall" (getRoomData state){room_desc = openedhall, exits = openedexits}))){door_closed = False}
{- | The state we change to when the user locks the door successfully -}
winstate :: GameData -> GameData
winstate state = (state){door_closed = True}

{- Don't update the game state, just list what the player is carrying -}
inv :: Command
inv state = (state, showInv (inventory state))
   where showInv [] = "You aren't carrying anything"
         showInv xs = "You are carrying:\n" ++ showInv' xs
         showInv' [] = ""
         showInv' [x] = show x
         showInv' (x:xs) = show x ++ "\n" ++ showInv' xs
{- | Help command which returns a game state with a help message ( 'helpStr' ) -}
help :: Command
help state = (state, helpStr)

{- | Quit command which sets the state finished bool to true which will end the loop in Adventure -}
quit :: Command
quit state = (state { finished = True }, "Bye bye")

{- | String displayed to the user which displays the possible commands -}
helpStr :: String
helpStr = "Commands: \n" ++
            "- inv\n" ++
            "  Displays all the items in your inventory\n" ++
            "\n" ++
            "- save {filename} e.g. save save_file1\n" ++
            "  Saves the current game state (Note: Don't add a file extension, .gd is added automatically\n" ++
            "\n" ++
            "- load {filename} e.g. load save_file1\n" ++
            "  Loads the game state from the save file\n" ++
            "\n" ++
            "- quit\n" ++
            "  Exits the game\n" ++
            "\n" ++
            "Actions: \n" ++
            "- go {direction} e.g. go north\n" ++
            "  Move to the next room in a given direction\n" ++
            "\n" ++
            "- get {object} e.g. get mug\n" ++
            "  Pick up the object and put it in your inventory\n" ++
            "\n" ++
            "- put {object} e.g. put mug\n" ++
            "  Remove the object from your inventory and place it in the current room\n" ++
            "\n" ++
            "- examine {object} e.g. examine mug\n" ++
            "  Shows a description of a given object\n" ++
            "\n" ++
            "- pour {coffee pot} e.g. pour pot\n" ++
            "  Pours the coffee giving you a full coffee mug (requires both the empty mug and the coffee pot in your inventory)\n" ++
            "\n" ++
            "- drink {coffee mug} e.g. drink coffee\n" ++
            "  Drinks the coffee, returning the empty mug to your inventory (requires full coffee mug)\n" ++
            "\n" ++
            "- open {door} e.g. open door\n" ++
            "  Opens the door to outside (requires a key)\n" ++
            "\n" ++
            "- close {door} e.g. close door\n" ++
            "  Closes the door\n" ++
            "\n" ++
            "Directions: north, south, east, west, out, in\n" ++
            "\n"