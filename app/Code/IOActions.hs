 module IOActions where



import Actions
import World
import System.IO
import System.Directory
-- DeepSeq and Exception are used for handling file contents in load
import Control.DeepSeq
import Control.Exception (evaluate)
-- readMaybe
import Text.Read


-- | Strings used for creating the file path
extension, fldr :: String
extension = ".gd"
fldr = "saves/"


{- The resources used in the saving and loading functionality are: 
Basic file handling with handles --> https://stackoverflow.com/questions/5053135/resource-busy-file-is-locked-error-in-haskell
Force evaluation of a file --> https://ecsdev.blogspot.com/2014/07/reading-and-writing-to-same-file-in.html
-}

-- | Writes a game state to a specified file
save :: Writes
save str state = do
                   writeHandle <- openFile (fldr ++ str ++ extension) WriteMode
                   hPutStr writeHandle (gdToString state)
                   hClose writeHandle
                   putStrLn ("Saved current game at " ++ fldr ++ str ++ extension ++ "!")
                   return ()

-- | Parses a game state to load into a new game. Default values are used for the GameData constructor if it can't be translated
load:: Reads
load str gd = do ok <- doesFileExist (fldr ++ str ++ extension)
                 let gdio = gd
                 if (ok) then do
                                -- Use a handle to read the file
                                -- readFile was too brittle and broke too easily
                                -- this provides control over when to close the file
                                handle <- openFile (fldr ++ str ++ extension) ReadMode
                                contents <- hGetContents handle
                                -- Gets contents as a list of strings
                                let list = lines contents
                                -- read the location_id
                                let loc = readLocation list
                                -- remove an element so now the list of objects are at the front
                                let dataO = removeElement list                                  
                                -- read the inventory
                                let objs = invent dataO
                                -- remove the inventory
                                let dataB = removeElement dataO
                                -- get the game data object
                                let gameData = GameData loc (getNewWorld objs) objs (readPoured dataB) (readCaffeinated dataB) (readFinished dataB) (readClosed dataB)
                                putStrLn ("Game save " ++ (fldr ++ str ++ extension) ++ " loaded!")
                                -- Evaluates the contents as hGetContents evaluates lazily
                                -- Without this line, return (gdio) wouldn't be legal
                                evaluate (rnf contents)
                                -- closes the handle
                                hClose handle
                                -- we can return this since we have evaluated the entire file content
                                return (gameData)
                         else do
                             -- fail
                                putStrLn ("Couldn't find game save: " ++ (fldr ++ str ++ extension) ++ ".")
                                return (gdio)


-- | reads the location_id by pattern matching the first line in the file
readLocation :: [String] -> String
readLocation [] = readRoom ""
readLocation (x:_) = readRoom x


-- | a safe tail function for removing the head element
removeElement :: [String] -> [String]
removeElement xs
    | null xs = []
    | otherwise = tail xs


-- | spits back the string and has a default location if it cannot match
readRoom :: String -> String
readRoom "kitchen" = "kitchen"
readRoom "bedroom" = "bedroom"
readRoom "living room" = "living room"
readRoom "street" = "street"
readRoom "hall"= "hall"
readRoom "toilet" = "toilet"
readRoom _ = "bedroom"

-- | Takes the first element of the list of strings and attempts to parse as a boolean
-- default value of False
readPoured :: [String] -> Bool
readPoured (x:_) = readBool x
readPoured _ = False


-- | attempts to reads the second element in the list as a bool
readCaffeinated :: [String] -> Bool
readCaffeinated (x:x':_) = readBool x'
readCaffeinated _ = False


-- | attempts to read the third element in the list as a bool
readFinished :: [String] -> Bool
readFinished (x:x':x'':_) = readBool x''
readFinished _ = False


-- | attempts to read the fourth element in the list as a bool
-- default value is True as the door has to be opened to go outside
readClosed :: [String] -> Bool
readClosed (x:x':x'':x''':_) = readBool x'''
readClosed _ = True


-- | pattern matches a string with a boolean
-- default value of false if it can't match
readBool :: String -> Bool
readBool "False" = False
readBool "True" = True
readBool _ = False


-- | This function loads the new game world by calling getNewRoom for each room in the world
getNewWorld :: [Object] -> [(String, Room)]
getNewWorld list = [("bedroom", getNewRoom list bedroom),
                    ("kitchen", getNewRoom list kitchen),
                    ("living room", getNewRoom list livingroom),
                    ("toilet", getNewRoom list toilet),
                    ("hall", getNewRoom list hall),
                    ("street", street)]

-- | Uses 'objectHere' in Actions.hs which removes any objects in a room if they are saved in an inventory
getNewRoom :: [Object] -> Room -> Room
getNewRoom [] rm = rm
getNewRoom (x:xs) rm = if (objectHere x rm) then getNewRoom xs (removeObject x rm)
                        else getNewRoom xs rm


-- | Takes a list of strings and attempts to parse as a list of objects
invent :: [String] -> [Object]
invent [] = []
-- calls readInv with a Maybe monad
invent (x:_) = readInv (readMaybe x :: Maybe [String])

-- | Pattern matches a maybe monad to convert to a list of objects
readInv :: Maybe [String] -> [Object]
-- Matches an empty list then return an empty list
readInv (Just []) = []
-- Map each string to each object in the list
readInv (Just xs) = map convertToObject xs
-- Default empty list
readInv Nothing = []


-- | Sets up the GameData as a string to be saved in a file
gdToString :: GameData -> String
gdToString gd =  location_id gd ++ "\n" ++
                 show (writeInv (inventory gd)) ++ "\n" ++
                 show (poured gd) ++ "\n" ++
                 show (caffeinated gd) ++ "\n" ++
                 show (finished gd) ++ "\n" ++
                 show (door_closed gd) ++ "\n"

-- Does the opposite of 'readInv' by mapping a list of objects to strings
writeInv :: [Object] -> [String]
writeInv list = map convertToString list

-- | Pattern matches objects and gives back a string representation
convertToString :: Object -> String
convertToString Mug = "Mug"
convertToString CoffeePot = "CoffeePot"
convertToString CoffeeMug = "CoffeeMug"
convertToString ToothBrush = "ToothBrush"
convertToString Key = "Key"
convertToString _ = "UnknownObject"

-- | Pattern matches strings and gives back the object representation
convertToObject :: String -> Object
convertToObject "Mug" = Mug
convertToObject "CoffeePot" = CoffeePot
convertToObject "CoffeeMug" = CoffeeMug
convertToObject "ToothBrush" = ToothBrush
convertToObject "Key" = Key
-- Just gives back a Mug in the event of an unknown object ( which should never happen )
-- We also can't have any UnknownObjects in the game world
convertToObject "UnknownObject" = Mug
convertToObject _ = Mug