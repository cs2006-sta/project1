module World where

-- | Exit object
data Exit = Exit { exit_dir :: String,
                   type_dir :: Direction,
                   exit_desc :: String,
                   room :: String }
   deriving Eq


-- | Room object
data Room = Room { room_desc :: String,
                   exits :: [Exit],
                   objects :: [Object] }
   deriving Eq


-- | Directions in the game world
data Direction = North | South | East | West | Out | In | UnknownDirection
   deriving (Eq)

-- |Custom show behaviour
instance Show Direction where
   show dir = case dir of
                  North   -> "north"
                  South   -> "south"
                  West    -> "west"
                  East    -> "east"
                  Out     -> "out"
                  In      -> "in"
                  _       -> "unknown"



-- | Objects appearing in the game world 
data Object = Mug | CoffeeMug | CoffeePot | UnknownObject | Door | Key | ToothBrush
   deriving Eq

-- | Custom show behaviour
instance Show Object where
   show obj = case obj of
                  Mug -> "An empty mug"
                  CoffeeMug -> "A full coffee mug"
                  CoffeePot -> "A pot of coffee"
                  Door -> "The front door"
                  Key -> "A silver door key"
                  ToothBrush -> "A tooth brush"
                  _ -> "unknown"


-- | Takes an object and returns a corresponding description
objectDescription :: Object -> String
objectDescription Mug = "An empty coffee mug"
objectDescription CoffeeMug = "A coffee mug containing freshly brewed coffee"
objectDescription CoffeePot = "A pot of freshly brewed coffee"
objectDescription Door = "The front door to your house"
objectDescription Key = "A silver door key with the engraving '42'"
objectDescription ToothBrush = "A blue toothbrush"
objectDescription _ = ""


-- | Information about the current state of the game
data GameData = GameData { location_id :: String, -- where player is
                           world :: [(String, Room)],
                           inventory :: [Object], -- objects player has
                           poured :: Bool, -- coffee is poured
                           caffeinated :: Bool, -- coffee is drunk
                           finished :: Bool, -- set to True at the end
                           door_closed :: Bool -- set to True at the end
                         }



-- | predefined show instance for the room object
instance Show Room where
    show (Room desc exits objs) = desc ++ "\n" ++ concatMap exit_desc exits ++
                                  showInv objs
       where showInv [] = ""
             showInv xs = "\n\nYou can see: " ++ showInv' xs
             showInv' [] = ""
             showInv' [x] = show x
             showInv' (x:xs) = show x ++ ", " ++ showInv' xs
                                  

-- | predefined show instance for 'GameData'
instance Show GameData where
    show gd = show (getRoomData gd)



-- | Things which do something to an object and update the game state
type Action  = Object -> GameData -> (GameData, String)


-- |New 'move' type takes a direction rather than an object ( used for the 'go' function )
type Move = Direction -> GameData -> (GameData, String)

-- | Things which just update the game state
type Command = GameData -> (GameData, String)


type Writes = String -> GameData -> IO ()
type Reads = String -> GameData -> IO GameData


-- Rooms with string representations
bedroom, kitchen, hall, street, livingroom, toilet :: Room


bedroom = Room "You are in your bedroom."
               [Exit "north" North "To the north is a kitchen. " "kitchen", 
               Exit "west" West "To the west is the toilet. " "toilet"]
               [Mug]

kitchen = Room "You are in the kitchen."
               [Exit "south" South "To the south is your bedroom. " "bedroom",
                Exit "west" West "To the west is a hallway. " "hall",
                Exit "east" East "To the east is the living room." "living room"]
               [CoffeePot]

hall = Room "You are in the hallway. The front door is closed. "
            [Exit "east" East "To the east is a kitchen. " "kitchen"]
            []

livingroom = Room "You are in the living room."
                  [Exit "west" West "To the west is the kitchen." "kitchen"]
                  [Key]
   
toilet = Room "You are in the toilet."
               [Exit "east" East"To the east is the bedroom." "bedroom"]
               [ToothBrush]


-- New data about the hall for when we open the door
openedhall :: String
openedhall = "You are in the hallway. The front door is open. "

openedexits :: [Exit]
openedexits = [Exit "east" East "To the east is a kitchen. " "kitchen",
               Exit "out" Out "You can go outside. " "street"]

street = Room "You have made it out of the house."
              [Exit "in" In "You can go back inside if you like. " "hall"]
              []

gameworld :: [(String, Room)]
-- | gameworld is a list of 'Room'
gameworld = [("bedroom", bedroom),
             ("kitchen", kitchen),
             ("hall", hall),
             ("street", street),
             ("living room", livingroom),
             ("toilet", toilet)]


-- | Returns the 'Room' the player is currently in
getRoomData :: GameData -> Room
getRoomData gd = maybe undefined id (lookup (location_id gd) (world gd))
