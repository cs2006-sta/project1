**** The source files can be found in app/Code.
     Documentation can be found in app/docs
     Game saves and the build can also be found in the app directory ****

     

     To launch the game you can do one of three things: 

     1. 
        Enter the Code directory and type:
        $ ghci

        The prelude should now load. If it does not make sure GHCI is installed. 
        Once the prelude is loaded, type:
        Prelude> :load Adventure

        The compiler will now compile the 6 file components. If the compiler cannot compile the files, please see the note at the bottom of the file
            OR 
            Run the game using step 3
        The game can be launched using:
        Adventure> main

        To run the non-haskeline solution (NOT RECOMMENDED) use:
        Adventure> main'


    2. 
        Enter the Code directory as before and type:
        $ ghc Adventure.hs

        (note the omission of the i)
        The executable file along with object files will now be created. If they cannot be created, see the note at the bottom of the file
            OR 
            Run the game using step 3
        The game can then be run by typing:
        $ ./Adventure

    3. (RECOMMENDED)
        Enter the app directory and type:
        $ cabal run

        Cabal will now build the project and run the executable that was build. 
	If this command cannot run the app then specify the executable to run:
	$ cabal run app
	
	The test suite can be run using the command:
	$ cabal run test
	OR
	$ cabal test

    

   NOTE: if the compiler fails to load the correct modules due to any imports then run this command inside app:
	 $ cabal install --dependencies-only
         OR do step 3 which will automatically install the required dependencies.     

    Enjoy!

    

    Authors: mdm22@st-andrews.ac.uk, jt228@st-andrews.ac.uk, mto1@st-andrews.ac.uk
